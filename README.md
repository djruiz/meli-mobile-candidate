### MELI - Mobile Candidate

El objetivo de esta etapa de la entrevista es desarrollar una app utilizando las APIs de Mercado Libre, que le permita a un usuario ver los detalles de un producto.

Para lograr esto, Mercado Libre posee APIs abiertas a la comunidad para que cualquier desarrollador las consuma y pueda tener búsquedas y compras en su aplicación.

** La app debería contar con tres pantallas: **
1. Campo de búsqueda.
2. Visualización de resultados de la búsqueda.
3. Detalle de un producto.
   Puedes entregar un listado y detalle de productos que sea puro texto, o un buscador con imágenes, iconos y texto, y un detalle completo del producto, como el que se muestra en la web.

**Requerimientos**
- Cada pantalla deberíamos poder rotarla y debería mantenerse el estado de la vista.
- Entrega del proyecto:
  Repositorio de código (GitHub público o similar).
- Manejo de casos de error desde el punto de vista del developer.
  Cómo se gestionan los casos de error inesperados, la consistencia a lo largo de toda la app, uso de logs, etc.
- Manejo de casos de error desde el punto de vista del usuario.
  Priorizar una experiencia fluida dando feedback al usuario adecuadamente.

## Screenshots

<img src="screenshots/1.png" alt="Screenshot">
<img src="screenshots/2.png" alt="Screenshot">

## Características
**Lista de artículos:**  Esta pantalla muestra una lista de artículos disponibles. Cada elemento de la lista puede contener información básica, como la imagen, el nombre del artículo y el precio . Los usuarios pueden desplazarse hacia arriba y hacia abajo para explorar la lista y seleccionar un artículo para ver más detalles.

**Página de detalles del artículo:** Cuando un usuario selecciona un artículo de la lista, se abre la pantalla de detalles del artículo. Aquí se muestra una descripción más detallada del artículo, junto con cualquier otra información relevante, como el precio, las especificaciones técnicas o las imágenes. _Los usuarios también pueden encontrar opciones adicionales, como agregar el artículo o compara el articulo, estas dos ultimas opciones no tienen funcionalidad_

**Página de búsqueda de artículos:** Esta pantalla permite a los usuarios buscar artículos específicos. Pueden ingresar palabras clave para refinar su búsqueda. Los resultados se mostrarán en una lista similar a la de la primera pantalla, y los usuarios podrán seleccionar un artículo para ver sus detalles.

### Manejo de casos de error desde el punto de vista del usuario.
**Vista sin conexión a Internet:** Cuando el usuario no tiene conexión a Internet, se mostrará una vista especial en la aplicación. Esta vista informará al usuario que no hay conexión disponible y ofrecerá algunas la opción, como volver a intentar la conexión.

**Vista de error de comunicación:** En caso de que ocurra un error durante la comunicación con los servidores a través de los servicios RESTful, se mostrará una vista de error. También puede proporcionar opciones para volver a intentar la solicitud o ponerse en contacto con el soporte técnico.

Estas dos vistas adicionales ayudan a mejorar la experiencia del usuario al proporcionar información clara sobre la falta de conexión a Internet o los errores de comunicación con los servidores. Así, los usuarios pueden tomar medidas adecuadas o intentar solucionar el problema de conexión antes de continuar utilizando la aplicación.

## Desarrollo de aplicaciones Android con Jetpack Compose, Inyección de Dependencias, Retrofit y Coil.
El código proporcionado muestra el desarrollo de aplicaciones Android utilizando tecnologías clave como Jetpack Compose, Inyección de Dependencias con Hilt, Retrofit y Coil. Con Jetpack Compose, se construyen interfaces de usuario dinámicas y reactivas de manera declarativa. La implementación de la Inyección de Dependencias con Hilt simplifica la gestión de dependencias y promueve la modularidad del código. Se utiliza Retrofit para realizar llamadas a una API y obtener datos, mientras que Coil se emplea como gestor de imágenes para cargar y mostrar imágenes de manera eficiente. Estas funciones combinadas permiten crear aplicaciones Android modernas, eficientes y visualmente atractivas.