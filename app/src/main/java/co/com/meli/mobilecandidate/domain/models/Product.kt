package co.com.meli.mobilecandidate.domain.models

import co.com.meli.mobilecandidate.data.source.remote.dto.Attribute

data class Product(
    val id: String,
    val title: String,
    val image: String,
    val price: String,
    val realPrice: String,
    val attributes: List<Attribute>,
)
