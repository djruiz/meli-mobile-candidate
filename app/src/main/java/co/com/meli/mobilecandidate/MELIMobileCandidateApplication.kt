package co.com.meli.mobilecandidate

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MELIMobileCandidateApplication: Application(){
}