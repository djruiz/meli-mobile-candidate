package co.com.meli.mobilecandidate.data.source.remote.dto

data class Value(
    val id: String,
    val name: String,
    val struct: Struct
)