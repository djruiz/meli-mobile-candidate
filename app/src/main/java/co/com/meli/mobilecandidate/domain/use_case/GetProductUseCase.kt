package co.com.meli.mobilecandidate.domain.use_case

import co.com.meli.mobilecandidate.data.Result
import co.com.meli.mobilecandidate.domain.models.Product
import co.com.meli.mobilecandidate.domain.repositories.SearchRepository
import javax.inject.Inject

class GetProductUseCase @Inject constructor(
    private val repository: SearchRepository
) {
    suspend operator fun invoke(id: String): Result<Product>{
        return repository.getProduct(id)
    }
}