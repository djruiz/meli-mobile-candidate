package co.com.meli.mobilecandidate.data.source.remote.dto

data class SearchLocation(
    val city: City,
    val neighborhood: Neighborhood,
    val state: State
)