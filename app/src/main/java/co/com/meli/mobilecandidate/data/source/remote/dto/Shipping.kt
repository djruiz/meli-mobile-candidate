package co.com.meli.mobilecandidate.data.source.remote.dto

import com.google.gson.annotations.SerializedName


data class Shipping(

    @SerializedName("store_pick_up") var storePickUp: Boolean? = null,
    @SerializedName("free_shipping") var freeShipping: Boolean? = null,
    @SerializedName("logistic_type") var logisticType: String? = null,
    @SerializedName("mode") var mode: String? = null,
    @SerializedName("tags") var tags: ArrayList<String> = arrayListOf(),
    @SerializedName("promise") var promise: String? = null

)