package co.com.meli.mobilecandidate.ui.shared

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import co.com.meli.mobilecandidate.R


@Composable
fun ImageComponent(
    image: ImageBitmap,
    contentDescription: String,
    modifier: Modifier = Modifier,
) {
    Image(
        bitmap = image,
        contentDescription = contentDescription,
        modifier = modifier,
    )
}

@Preview
@Composable
private fun PreviewComponent() {
    val image = ImageBitmap.imageResource(R.drawable.ofline)
    ImageComponent(
        image = image,
        contentDescription = "null",
        modifier = Modifier
            .fillMaxWidth()
            .height(100.dp)
    )
}