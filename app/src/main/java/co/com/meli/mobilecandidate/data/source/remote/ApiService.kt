package co.com.meli.mobilecandidate.data.source.remote

import co.com.meli.mobilecandidate.data.source.remote.dto.ProductDto
import co.com.meli.mobilecandidate.data.source.remote.dto.SearchDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("/sites/MLA/search")
    suspend fun getProducts(
        @Query("q") str: String
    ): SearchDto

    @GET("/items/{id}")
    suspend fun getProduct(
        @Path("id") id: String
    ): ProductDto


}