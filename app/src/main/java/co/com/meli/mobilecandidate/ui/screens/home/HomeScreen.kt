package co.com.meli.mobilecandidate.ui.screens.home

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.LiveData
import androidx.navigation.NavHostController
import co.com.meli.mobilecandidate.domain.models.Products
import co.com.meli.mobilecandidate.ui.shared.ProductComponent
import co.com.meli.mobilecandidate.ui.theme.bg
import co.com.meli.mobilecandidate.ui.theme.black
import kotlinx.coroutines.flow.collectLatest
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import co.com.meli.mobilecandidate.navigation.AppScreens
import co.com.meli.mobilecandidate.ui.screens.detail.DetailViewModel
import co.com.meli.mobilecandidate.ui.shared.ErrorComponent
import co.com.meli.mobilecandidate.ui.shared.OfflineComponent

@Composable
fun HomeScreen(
    navController: NavHostController,
    str: String,
    viewModel: HomeViewModel = hiltViewModel()
) {

    val state = viewModel.state

    val eventFlow = viewModel.eventFlow
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true) {
        eventFlow.collectLatest { event ->
            when (event) {
                is HomeViewModel.UIEvent.ShowSnackBar -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = event.message
                    )
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = { Toolbar(navController, str = state.str) },
        content = { innerPadding ->
            HomeContent(
                modifier = Modifier.padding(innerPadding),
                viewModel = viewModel,
                isLoading = state.isLoading,
                isDisconnectInternet = state.isDisconnectInternet,
                isError = state.isError,
                navController,
                products = state.products
            )
        }
    )
}

@Composable
fun Toolbar(navController: NavController, str: String) {
    TopAppBar(
        title = {
            Box(
                modifier = Modifier
                    .background(Color.White, RoundedCornerShape(16.dp))
                    .padding(horizontal = 16.dp)
                    .fillMaxWidth()
                    .height(30.dp)
                    .clickable { goSearch(navController) }
            ) {
                Row(
                    modifier = Modifier.fillMaxHeight(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        imageVector = Icons.Default.Search,
                        contentDescription = null, // Puedes proporcionar una descripción adecuada
                        tint = Color.Gray, // Color del icono
                        modifier = Modifier.width(18.dp)
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(
                        text = str,
                        fontSize = 12.sp,
                        color = Color.Gray,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier.align(Alignment.CenterVertically)

                    )
                }
            }
        },
        navigationIcon = {
            IconButton(onClick = { /* Acción al hacer clic en el ícono de menú */ }) {
                Icon(Icons.Default.Menu, contentDescription = "Menú")
            }
        },
        actions = {
            IconButton(onClick = { /* Acción al hacer clic en el primer ícono de acción */ }) {
                Icon(Icons.Default.ShoppingCart, contentDescription = "")
            }
        },
        backgroundColor = bg,
        contentColor = black
    )
}

@Composable
fun HomeContent(
    modifier: Modifier = Modifier,
    viewModel: HomeViewModel,
    isLoading: Boolean = false,
    isDisconnectInternet: Boolean = false,
    isError: Boolean = false,
    navController: NavController,
    products: List<Products> = emptyList()
) {
    Surface(
        modifier = modifier.fillMaxSize(),
        color = MaterialTheme.colors.surface
    ) {
        LazyColumn(
            contentPadding = PaddingValues(vertical = 6.dp),
            modifier = Modifier.fillMaxWidth(),
            content = {
                items(products.size) { index ->
                    ProductComponent(
                        modifier = Modifier.fillMaxWidth(),
                        navController,
                        item = products[index],
                    )
                }
            }
        )

        if (isLoading) {
            FullScreenLoading()
        }

        if (isDisconnectInternet) {
            OfflineComponent { viewModel.refreshProduct() }
        }

        if (isError) {
            ErrorComponent { viewModel.refreshProduct() }
        }
    }

}

@Preview
@Composable
private fun FullScreenLoading() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
    ) {
        CircularProgressIndicator(color = bg)
    }
}

private fun goSearch(navController: NavController){
    navController.navigate(AppScreens.SearchScreen.route)
}