package co.com.meli.mobilecandidate.data.source.remote.dto

import co.com.meli.mobilecandidate.domain.models.Products
import com.google.gson.annotations.SerializedName


data class SearchDto(

    @SerializedName("site_id") var siteId: String? = null,
    @SerializedName("country_default_time_zone") var countryDefaultTimeZone: String? = null,
    @SerializedName("query") var query: String? = null,
    @SerializedName("paging") var paging: Paging? = Paging(),
    @SerializedName("results") var results: ArrayList<Results> = arrayListOf(),
    @SerializedName("sort") var sort: Sort? = Sort(),
    @SerializedName("available_sorts") var availableSorts: ArrayList<AvailableSorts> = arrayListOf(),
    @SerializedName("available_filters") var availableFilters: ArrayList<AvailableFilters> = arrayListOf()
)


fun SearchDto.toListProducts(): List<Products> {
    val resultEntries = results.mapIndexed { _, entries ->
        Products(
            id = entries.id!!,
            title = entries.title!!,
            image = entries.thumbnail!!,
            price = entries.price!!,
        )
    }
    return resultEntries
}

