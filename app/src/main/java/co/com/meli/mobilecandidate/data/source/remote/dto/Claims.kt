package co.com.meli.mobilecandidate.data.source.remote.dto

import com.google.gson.annotations.SerializedName


data class Claims(

    @SerializedName("period") var period: String? = null,
    @SerializedName("rate") var rate: Double? = null,
    @SerializedName("value") var value: Int? = null

)