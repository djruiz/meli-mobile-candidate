package co.com.meli.mobilecandidate.data.source.remote.dto

data class Neighborhood(
    val id: String,
    val name: String
)