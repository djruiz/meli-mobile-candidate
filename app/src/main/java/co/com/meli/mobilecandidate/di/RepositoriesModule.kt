package co.com.meli.mobilecandidate.di

import co.com.meli.mobilecandidate.data.repositories.SearchRepositoryImp
import co.com.meli.mobilecandidate.domain.repositories.SearchRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoriesModule {

    @Binds
    abstract fun bindSearchRepository(impl: SearchRepositoryImp): SearchRepository
}