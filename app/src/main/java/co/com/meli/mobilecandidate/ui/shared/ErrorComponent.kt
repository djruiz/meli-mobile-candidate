package co.com.meli.mobilecandidate.ui.shared

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import co.com.meli.mobilecandidate.R
import co.com.meli.mobilecandidate.di.RemoteModule
import co.com.meli.mobilecandidate.ui.theme.lightBlue600
import co.com.meli.mobilecandidate.util.InternetUtil

@Composable
fun ErrorComponent(onRetry: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White),
        verticalArrangement = Arrangement.SpaceBetween,
    ) {
        Box() {}
        Box() {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                ImageOffline()
                Title()
                Spacer(modifier = Modifier.height(10.dp))
                Caption()
            }
        }
        Box(modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp), Alignment.Center) {
            ButtonRetry(onRetry = onRetry)
        }

    }

}

@Composable
private fun ImageOffline() {
    Image(
        painter = painterResource(id = R.drawable.error),
        contentDescription = "",
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
    )
}

@Composable
private fun Title() {
    Text(
        text = stringResource(id = R.string.error_handler_core_server_title),
        style = MaterialTheme.typography.h6,
        textAlign = TextAlign.Center
    )
}

@Composable
private fun Caption() {
    Text(
        text = stringResource(id = R.string.error_handler_core_server_subtitle),
        style = MaterialTheme.typography.caption,
        textAlign = TextAlign.Center
    )
}

@Composable
private fun ButtonRetry(onRetry: () -> Unit) {

    Button(
        onClick = { onRetry() },
        Modifier
            .background(Color.White)
            .fillMaxWidth(),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.White,
            contentColor = lightBlue600.copy(1f)
        ),
        elevation = null
    ) {
        Text(text = stringResource(id = R.string.error_handler_core_retry_button))
    }
}

