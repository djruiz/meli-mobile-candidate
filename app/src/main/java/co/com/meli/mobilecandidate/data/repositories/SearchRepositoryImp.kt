package co.com.meli.mobilecandidate.data.repositories

import co.com.meli.mobilecandidate.data.Result
import co.com.meli.mobilecandidate.data.source.remote.ApiService
import co.com.meli.mobilecandidate.data.source.remote.dto.toListProducts
import co.com.meli.mobilecandidate.data.source.remote.dto.toProduct
import co.com.meli.mobilecandidate.domain.models.Product
import co.com.meli.mobilecandidate.domain.models.Products
import co.com.meli.mobilecandidate.domain.repositories.SearchRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception
import javax.inject.Inject

class SearchRepositoryImp @Inject constructor(
    private val api: ApiService
) : SearchRepository {
    override fun getProducts(str: String): Flow<Result<List<Products>>> = flow {
        emit(Result.Loading())
        try {
            val response = api.getProducts(str).toListProducts()
            emit(Result.Success(response))
        } catch (e: HttpException) {
            emit(
                Result.Error(
                    message = "Oops, something went wrong",
                    data = null
                )
            )
        } catch (e: IOException) {
            emit(
                Result.Error(
                    message = "Couldn't reach server, check your internet connection",
                    data = null
                )
            )
        } catch (e: Exception) {
            emit(
                Result.Error(
                    message = "Otro error",
                    data = null
                )
            )
        }
    }

    override suspend fun getProduct(id: String): Result<Product> {
        val response = try {
            api.getProduct(id)
        } catch (e: Exception) {
            return Result.Error("An unknown error occurred")
        }
        return Result.Success(response.toProduct())
    }
}


