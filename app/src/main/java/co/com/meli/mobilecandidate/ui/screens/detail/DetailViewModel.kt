package co.com.meli.mobilecandidate.ui.screens.detail

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import co.com.meli.mobilecandidate.data.Result
import co.com.meli.mobilecandidate.domain.use_case.GetProductUseCase
import co.com.meli.mobilecandidate.util.InternetUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val getProductUseCase: GetProductUseCase,
    private val savedStateHandle: SavedStateHandle,
    application: Application
) : AndroidViewModel(application) {

    var state by mutableStateOf(DetailState(isLoading = true))
        private set

    private val _eventFlow = MutableSharedFlow<UIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        refreshProduct()
    }


    private fun getProduct() {
        savedStateHandle.get<String>("id")?.let { productId ->
            viewModelScope.launch {
                getProductUseCase(productId).also { result ->
                    when (result) {

                        is Result.Success -> {
                            isLoading(false)
                            state = state.copy(product = result.data)
                        }

                        is Result.Error -> {
                            isError()
                            Log.d("DetailViewModel", "getProduct: ${result.message ?: "Unknown error"}")
                        }

                        is Result.Loading -> {
                            isLoading()
                        }

                    }
                }
            }
        }
    }


    sealed class UIEvent {
        data class ShowSnackBar(val message: String) : UIEvent()
    }

    fun refreshProduct() {
        isLoading()
        if (InternetUtil.isInternetAvailable(context = getApplication())) {
            getProduct()
        } else {
            isOffline()
            Log.d("DetailViewModel", "getProduct: Usuario sin conexción a internet")

        }
    }

    private fun isLoading(isLoading: Boolean = true) {
        state = state.copy(
            isLoading = isLoading,
            isDisconnectInternet = false,
            isError = false
        )
    }

    private fun isOffline(isOffline: Boolean = true) {
        state = state.copy(
            isLoading = false,
            isDisconnectInternet = isOffline,
            isError = false
        )
    }

    private fun isError(isError: Boolean = true) {
        state = state.copy(
            isLoading = false,
            isDisconnectInternet = false,
            isError = isError
        )
    }

}

