package co.com.meli.mobilecandidate.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import co.com.meli.mobilecandidate.ui.screens.detail.DetailScreen
import co.com.meli.mobilecandidate.ui.screens.home.HomeScreen
import co.com.meli.mobilecandidate.ui.screens.search.SearchScreen
import co.com.meli.mobilecandidate.ui.screens.splash.SplashScreen

@Composable
fun AppNavigation() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = AppScreens.SplashScreen.route
    ) {
        composable(AppScreens.SplashScreen.route) {
            SplashScreen(navController)
        }
        composable(
            route = "${AppScreens.HomeScreen.route}/{str}",
            arguments = listOf(navArgument("str") { type = NavType.StringType })
        ) { entry ->
            val str = entry.arguments?.getString("str") ?: ""
            HomeScreen(navController, str)
        }
        composable(
            route = "${AppScreens.DetailScreen.route}/{id}",
            arguments = listOf(navArgument("id") { type = NavType.StringType })
        ) { entry ->
            val id = entry.arguments?.getString("id") ?: ""
            DetailScreen(navController, id)
        }
        composable(AppScreens.SearchScreen.route){
            SearchScreen(navController = navController)
        }
    }
}