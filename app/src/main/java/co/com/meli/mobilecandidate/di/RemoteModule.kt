package co.com.meli.mobilecandidate.di

import android.content.Context
import co.com.meli.mobilecandidate.data.source.remote.ApiService
import co.com.meli.mobilecandidate.util.BASE_URL
import co.com.meli.mobilecandidate.util.InternetUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.flow.MutableStateFlow
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object RemoteModule {

    private val connectionStateFlow: MutableStateFlow<Boolean> = MutableStateFlow(false)

    @Provides
    @Singleton
    fun provideConnectionStateFlow(): MutableStateFlow<Boolean> {
        return connectionStateFlow
    }

    fun setConnectionState(connected: Boolean) {
        connectionStateFlow.value = connected
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(@ApplicationContext context: Context): OkHttpClient {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(Interceptor { chain ->
                val request: Request = chain.request()

                val isConnected = InternetUtil.isInternetAvailable(context)

                if (isConnected) {
                    setConnectionState(true)
                } else {
                    setConnectionState(false)
                }

                val response: Response = chain.proceed(request)
                val responseBody = response.body?.string()

                //setConnectionState(true)

                // Reconstruir la respuesta para evitar problemas al consumirla en otros lugares
                response.newBuilder()
                    .body(responseBody?.toResponseBody(response.body?.contentType()))
                    .build()
            })
            .build()
    }


    @Provides
    @Singleton
    fun provideMeli(okHttpClient: OkHttpClient): ApiService {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }
}