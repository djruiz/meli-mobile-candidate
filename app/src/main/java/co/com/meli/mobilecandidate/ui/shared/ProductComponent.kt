package co.com.meli.mobilecandidate.ui.shared

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import co.com.meli.mobilecandidate.domain.models.Products
import co.com.meli.mobilecandidate.navigation.AppScreens
import co.com.meli.mobilecandidate.ui.theme.gray500
import co.com.meli.mobilecandidate.ui.theme.greenA700
import co.com.meli.mobilecandidate.ui.theme.lightGreenA700
import co.com.meli.mobilecandidate.util.formatPrice
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import co.com.meli.mobilecandidate.R

@Composable
fun ProductComponent(modifier: Modifier = Modifier, navController: NavController, item: Products) {

    Row(
        modifier = modifier
            .clickable {
                performActionOnClick(
                    navController,
                    item
                )
            } // Acción al hacer clic en el componente
            .padding(start = 6.dp, top = 12.dp, bottom = 12.dp)
    ) {
        ProductImage(item)
        Spacer(Modifier.width(20.dp))
        ProductDetails(item)
    }

    Divider(modifier = Modifier, thickness = 0.8.dp)

}

@Composable
fun ProductDetails(item: Products) {
    Column(
        modifier = Modifier
            .fillMaxWidth(),
            verticalArrangement = Arrangement.Top
    ) {
        Text(
            text = item.title,
            style = MaterialTheme.typography.body2
        )
        Spacer(Modifier.height(5.dp))
        Text(
            text = item.price.formatPrice(),
            style = MaterialTheme.typography.h6,
            fontWeight = FontWeight(400)
        )
        Spacer(Modifier.height(5.dp))
        Text(
            text = stringResource(id = R.string.envio_gratis),
            style = MaterialTheme.typography.body2,
            fontSize = 12.sp,
            color = greenA700,
            fontWeight = FontWeight(500)
        )
    }
}

// This function may be private
@Composable
fun ProductImage(item: Products) {
    ProductImageContainer(
        modifier = Modifier
            .width(100.dp)
            .height(120.dp)
    ) {
        Box(modifier = Modifier.fillMaxSize()) {
            val painter = rememberAsyncImagePainter(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(item.image)
                    .size(Size.ORIGINAL)
                    .build()
            )
            Image(
                painter = painter,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize()
            )
        }
    }
}

// This function may be private
@Composable
fun ProductImageContainer(
    modifier: Modifier,
    content: @Composable () -> Unit
) {
    Surface(
        modifier
            .background(gray500)
            .aspectRatio(1f), RoundedCornerShape(4.dp)) {
        content()
    }
}

private fun performActionOnClick(navController: NavController, item: Products) {
    navController.navigate("${AppScreens.DetailScreen.route}/${item.id}")
}