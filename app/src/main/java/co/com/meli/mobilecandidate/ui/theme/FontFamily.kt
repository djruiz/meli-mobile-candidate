package co.com.meli.mobilecandidate.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import co.com.meli.mobilecandidate.R


val nova = FontFamily(
    Font(R.font.proxima_nova_black, FontWeight.Black),
    Font(R.font.proxima_nova_bold, FontWeight.Bold),
    Font(R.font.proxima_nova_extrabold, FontWeight.ExtraBold),
    Font(R.font.proxima_nova_light, FontWeight.Light),
    Font(R.font.proxima_nova_medium, FontWeight.Medium),
    Font(R.font.proxima_nova_semibold, FontWeight.SemiBold),
    Font(R.font.proxima_nova_thin, FontWeight.Thin),
    Font(R.font.proxima_nova_regular, FontWeight.Normal)
)