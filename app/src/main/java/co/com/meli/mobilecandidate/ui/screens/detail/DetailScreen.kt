package co.com.meli.mobilecandidate.ui.screens.detail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import co.com.meli.mobilecandidate.R
import co.com.meli.mobilecandidate.domain.models.Product
import co.com.meli.mobilecandidate.navigation.AppScreens
import co.com.meli.mobilecandidate.ui.shared.ErrorComponent
import co.com.meli.mobilecandidate.ui.shared.OfflineComponent
import co.com.meli.mobilecandidate.ui.theme.bg
import co.com.meli.mobilecandidate.ui.theme.black
import co.com.meli.mobilecandidate.ui.theme.blue100
import co.com.meli.mobilecandidate.ui.theme.blue700
import co.com.meli.mobilecandidate.ui.theme.gray100
import co.com.meli.mobilecandidate.ui.theme.gray500
import co.com.meli.mobilecandidate.ui.theme.gray800
import co.com.meli.mobilecandidate.ui.theme.gray900
import co.com.meli.mobilecandidate.ui.theme.nova
import co.com.meli.mobilecandidate.ui.theme.white
import co.com.meli.mobilecandidate.util.formatPrice
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Size
import kotlinx.coroutines.flow.collectLatest

@Composable
fun DetailScreen(
    navController: NavHostController,
    id: String = "",
    viewModel: DetailViewModel = hiltViewModel()
) {

    val state = viewModel.state
    val eventFlow = viewModel.eventFlow
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true) {
        eventFlow.collectLatest { event ->
            when (event) {
                is DetailViewModel.UIEvent.ShowSnackBar -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = event.message
                    )
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = { Toolbar(navController) },
        content = { innerPadding ->
            HomeContent(
                modifier = Modifier.padding(innerPadding),
                viewModel = viewModel,
                isLoading = state.isLoading,
                isDisconnectInternet = state.isDisconnectInternet,
                isError = state.isError,
                product = state.product,
            )
        }
    )
}

@Composable
fun Toolbar(navController: NavController) {
    TopAppBar(
        title = {
            Text(
                text = "Producto",
                fontSize = 16.sp,
                color = gray900,
                fontWeight = FontWeight.Normal,
                modifier = Modifier,
            )
        },
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                Icon(Icons.Default.ArrowBack, contentDescription = "Back")
            }
        },
        actions = {
            IconButton(onClick = { /* Acción al hacer clic en el primer ícono de acción */ }) {
                Icon(Icons.Default.ShoppingCart, contentDescription = "")
            }
        },
        backgroundColor = bg,
        contentColor = black
    )
}

@Composable
fun HomeContent(
    modifier: Modifier = Modifier,
    viewModel: DetailViewModel,
    isLoading: Boolean = false,
    isDisconnectInternet: Boolean = false,
    isError: Boolean = false,
    product: Product?,
) {
    Surface(
        modifier = modifier.fillMaxSize(),
        color = MaterialTheme.colors.surface
    ) {

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
        ) {

            item {
                Column(Modifier.padding(16.dp)) {
                    if (product != null) {
                        Title(product = product)
                        Spacer(modifier = Modifier.height(5.dp))
                        ProductImage(image = product.image)
                        Prices(product = product)
                        Spacer(modifier = Modifier.height(16.dp))
                        Attributes(product = product)
                        Spacer(modifier = Modifier.height(16.dp))
                        Buttons()
                    }
                }
            }
        }

        if (isLoading) {
            FullScreenLoading()
        }

        if (isDisconnectInternet) {
            OfflineComponent { viewModel.refreshProduct() }
        }

        if (isError) {
            ErrorComponent { viewModel.refreshProduct() }
        }
    }

}

@Preview
@Composable
private fun FullScreenLoading() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
    ) {
        CircularProgressIndicator(color = bg)
    }
}

@Composable
fun ProductImage(image: String?) {
    ImageContainer(modifier = Modifier.fillMaxWidth()) {
        Box {
            val painter = rememberAsyncImagePainter(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(image)
                    .size(Size.ORIGINAL)
                    .build()
            )
            Image(
                painter = painter,
                contentDescription = null,
                contentScale = ContentScale.Fit,
                modifier = Modifier.fillMaxSize()
            )
        }
    }
}

@Composable
private fun ImageContainer(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Surface(modifier.aspectRatio(1f)) {
        content()
    }
}

@Composable
private fun Attributes(product: Product) {
    product.attributes.forEach {
        if (it.attribute_group_id != "OTHERS") {
            Column(
                modifier = Modifier
                    .background(gray100, shape = RoundedCornerShape(4.dp))
                    .padding(8.dp)
                    .fillMaxWidth(),
            ) {
                Text(
                    text = it.name,
                    fontFamily = nova,
                    color = gray800,
                    fontWeight = FontWeight(300),
                    style = MaterialTheme.typography.body1
                )
                Text(
                    text = it.value_name,
                    fontFamily = nova,
                    color = gray800,
                    fontWeight = FontWeight(600),
                    style = MaterialTheme.typography.body2
                )
            }
            Spacer(modifier = Modifier.height(8.dp))
        }

    }
}

@Composable
private fun Buttons() {
    Button(modifier = Modifier.fillMaxWidth(),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = blue700,
            contentColor = white
        ),
        onClick = {}) {
        Text(
            text = stringResource(R.string.txt_comprar_ahora),
            modifier = Modifier.padding(8.dp, 8.dp)
        )
    }

    Spacer(modifier = Modifier.height(8.dp))

    Button(modifier = Modifier.fillMaxWidth(),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = blue100,
            contentColor = blue700
        ),
        onClick = {}) {
        Text(
            text = stringResource(R.string.txt_agregar_al_carrito),
            modifier = Modifier.padding(8.dp, 8.dp)
        )
    }
}

@Composable
private fun Prices(product: Product) {
    Text(
        text = product.price.formatPrice(),
        fontFamily = nova,
        color = gray500,
        textDecoration = TextDecoration.LineThrough,
        fontWeight = FontWeight(400),
        style = MaterialTheme.typography.body2
    )
    Spacer(modifier = Modifier.height(5.dp))
    Text(
        text = product.price.formatPrice(),
        fontFamily = nova,
        color = gray800,
        fontWeight = FontWeight(500),
        style = MaterialTheme.typography.h4
    )
}

@Composable
private fun Title(product: Product) {
    Text(
        text = stringResource(R.string.txt_nuevo),
        fontFamily = nova,
        color = gray500,
        fontWeight = FontWeight(400),
        style = MaterialTheme.typography.caption
    )
    Text(
        text = product.title,
        fontFamily = nova,
        color = gray800,
        fontWeight = FontWeight(400),
        style = MaterialTheme.typography.body1
    )
}
