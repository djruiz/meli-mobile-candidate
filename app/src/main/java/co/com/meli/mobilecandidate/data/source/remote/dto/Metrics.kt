package co.com.meli.mobilecandidate.data.source.remote.dto

import com.google.gson.annotations.SerializedName


data class Metrics(

    @SerializedName("sales") var sales: Sales? = Sales(),
    @SerializedName("claims") var claims: Claims? = Claims(),
    @SerializedName("delayed_handling_time") var delayedHandlingTime: DelayedHandlingTime? = DelayedHandlingTime(),
    @SerializedName("cancellations") var cancellations: Cancellations? = Cancellations()

)