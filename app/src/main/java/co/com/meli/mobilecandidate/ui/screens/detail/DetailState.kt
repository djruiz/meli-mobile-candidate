package co.com.meli.mobilecandidate.ui.screens.detail

import co.com.meli.mobilecandidate.domain.models.Product
import co.com.meli.mobilecandidate.domain.models.Products

data class DetailState(
    var str: String = "",
    val product: Product? = null,
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val isDisconnectInternet: Boolean = false,
)
