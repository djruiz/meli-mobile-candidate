package co.com.meli.mobilecandidate.ui.screens.search

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import co.com.meli.mobilecandidate.R
import co.com.meli.mobilecandidate.navigation.AppScreens
import co.com.meli.mobilecandidate.ui.theme.*

@Composable
fun SearchScreen(
    navController: NavHostController,
    viewModel: SearchViewModel = hiltViewModel()
) {

    val scaffoldState = rememberScaffoldState()

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = { Toolbar(navController) },
        content = { innerPadding ->
            SearchContent(
                modifier = Modifier.padding(innerPadding)
            )
        }
    )

}

@Composable
fun SearchContent(modifier: Modifier) {

}

@Composable
fun Toolbar(navController: NavHostController) {
    var searchQuery by remember { mutableStateOf("") }
    val focusRequester = remember { FocusRequester() }

    LaunchedEffect(Unit) {
        focusRequester.requestFocus() // Establece el enfoque automático al cargar la vista
    }

    TopAppBar(
        title = {
            TextField(
                value = searchQuery,
                onValueChange = { newValue -> searchQuery = newValue },
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White)
                    .border(0.dp, Color.Transparent)
                    .focusRequester(focusRequester),
                singleLine = true,
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Search // Configuración para mostrar el teclado de búsqueda
                ),
                keyboardActions = KeyboardActions(
                    onSearch = {
                        performSearch(
                            navController,
                            searchQuery
                        ) // Acción a realizar cuando se presiona el botón de búsqueda
                    }
                ),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.White,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    textColor = gray500
                ),
                textStyle = TextStyle(
                    fontFamily = nova,
                    fontWeight = FontWeight(300),
                ),
                placeholder = {
                    Text(
                        text = stringResource(id = R.string.buscar_en_mercado_libre),
                        fontFamily = nova,
                        fontWeight = FontWeight(300),
                        color = gray500
                    )
                },

                )
        },
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                Icon(Icons.Default.ArrowBack, contentDescription = "Back")
            }
        },
        actions = {
            IconButton(onClick = { /* Acción al hacer clic en el primer ícono de acción */ }) {
                Icon(Icons.Default.ShoppingCart, contentDescription = "")
            }
        },
        backgroundColor = white,
        contentColor = black
    )
}

fun performSearch(navController: NavHostController, searchQuery: String) {
    navController.navigate("${AppScreens.HomeScreen.route}/${searchQuery}")
}
