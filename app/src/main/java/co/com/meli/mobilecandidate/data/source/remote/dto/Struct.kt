package co.com.meli.mobilecandidate.data.source.remote.dto

data class Struct(
    val number: Double,
    val unit: String
)