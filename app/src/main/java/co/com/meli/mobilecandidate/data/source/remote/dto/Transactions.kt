package co.com.meli.mobilecandidate.data.source.remote.dto

import com.google.gson.annotations.SerializedName


data class Transactions(

    @SerializedName("canceled") var canceled: Int? = null,
    @SerializedName("completed") var completed: Int? = null,
    @SerializedName("period") var period: String? = null,
    @SerializedName("ratings") var ratings: Ratings? = Ratings(),
    @SerializedName("total") var total: Int? = null

)