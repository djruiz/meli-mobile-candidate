package co.com.meli.mobilecandidate.data.source.remote.dto

data class ValueStruct(
    val number: Double,
    val unit: String
)