package co.com.meli.mobilecandidate.util

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale


fun String.formatPrice(): String {
    val numberFormat = NumberFormat.getNumberInstance(Locale.getDefault())
    val decimalFormatSymbols = (numberFormat as DecimalFormat).decimalFormatSymbols

    decimalFormatSymbols.decimalSeparator = if (decimalFormatSymbols.decimalSeparator == ',') '.' else ','
    (numberFormat as DecimalFormat).decimalFormatSymbols = decimalFormatSymbols

    numberFormat.maximumFractionDigits = 2
    numberFormat.minimumFractionDigits = 2
    numberFormat.currency = Currency.getInstance("USD")

    return "$ " + numberFormat.format(this.toDouble())
}