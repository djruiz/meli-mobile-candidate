package co.com.meli.mobilecandidate.ui.screens.home

import co.com.meli.mobilecandidate.domain.models.Products

data class HomeState(
    var str: String = "",
    val products: List<Products> = emptyList(),
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val isDisconnectInternet: Boolean = false,
)
