package co.com.meli.mobilecandidate.domain.models

data class Products(
    val id: String,
    val title: String,
    val image: String,
    val price: String
)
