package co.com.meli.mobilecandidate.domain.repositories

import co.com.meli.mobilecandidate.data.Result
import co.com.meli.mobilecandidate.domain.models.Product
import co.com.meli.mobilecandidate.domain.models.Products
import kotlinx.coroutines.flow.Flow

interface SearchRepository {
    fun getProducts(str: String): Flow<Result<List<Products>>>
    suspend fun getProduct(id: String): Result<Product>
}