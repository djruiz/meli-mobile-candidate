package co.com.meli.mobilecandidate.navigation

sealed class AppScreens(var route: String) {
    object SplashScreen: AppScreens("splash_screen")
    object HomeScreen: AppScreens("home_screen")
    object SearchScreen: AppScreens("search_screen")
    object DetailScreen: AppScreens("detail_screen")
}