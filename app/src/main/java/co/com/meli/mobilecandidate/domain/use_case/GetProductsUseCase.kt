package co.com.meli.mobilecandidate.domain.use_case

import co.com.meli.mobilecandidate.data.Result
import co.com.meli.mobilecandidate.domain.models.Products
import co.com.meli.mobilecandidate.domain.repositories.SearchRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetProductsUseCase @Inject constructor(
    private val repository: SearchRepository
) {
    operator fun invoke(str: String): Flow<Result<List<Products>>>{
        return repository.getProducts(str)
    }
}